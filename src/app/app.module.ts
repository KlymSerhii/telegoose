import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Geolocation } from '@ionic-native/geolocation';

import { MyApp } from './app.component';
import {UserService} from "../services/user.service";
import {TripService} from "../services/trip.service";
import {HttpModule} from "@angular/http";
import { IonicStorageModule } from '@ionic/storage';
import { Angular2SocialLoginModule } from "angular2-social-login";
import { AuthService } from "angular2-social-login";
import {SocketService} from "../services/socket.service";

let providers = {
  "facebook": {
    "clientId": "210673319454518",
    "apiVersion": "v2.4" //like v2.4
  }
};

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Geolocation,
    AuthService,
    UserService,
    TripService,
    SocketService,
  ]
})
export class AppModule {}

Angular2SocialLoginModule.loadProvidersScripts(providers);
