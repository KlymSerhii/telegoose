import {Input, Directive} from '@angular/core';
import {GoogleMapsAPIWrapper} from 'angular2-google-maps/core';
import {TripService} from "../services/trip.service";

declare var google: any;
@Directive({
  selector: 'sebm-google-map-directions',
})

export class DirectionsMapDirective {
  @Input() origin: any;
  @Input() destination: any;
  @Input() directionsDisplay: any;

  constructor(public mapsAPIWrapper: GoogleMapsAPIWrapper,
              private tripService: TripService) {
  }


  updateRoutes(origin: any, destination: any, waypoints: any) {
    this.mapsAPIWrapper.getNativeMap().then(map => {
      const self = this;
      this.origin = new google.maps.LatLng(origin.lat, origin.lng);
      this.destination = new google.maps.LatLng(destination.lat, destination.lng);
      let parsedWaypoints: any[] = waypoints.map((point) => {
        return {
          location: new google.maps.LatLng(point.lat, point.lng),
          stopover: true
        };
      });
      let directionsService = new google.maps.DirectionsService;
      let directionsDisplay = new google.maps.DirectionsRenderer;
      directionsDisplay.setMap(map);
      directionsService.route({
        origin: this.origin,
        destination: this.destination,
        waypoints: parsedWaypoints,
        optimizeWaypoints: true,
        travelMode: 'DRIVING',

      }, function (res: any, status: any) {
        if (status === 'OK') {
          console.log("STATUS WAS OK");
          directionsDisplay.setDirections(res);
          self.tripService.saveTripLocal({markers: waypoints});
        } else {
          window.alert('Directions request failed due to ' + status);
        }
      });
    });
  }
}
