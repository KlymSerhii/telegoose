import {marker} from "./marker.model";
export class DriverTripModel {
  tripId: string;
  startPosition:marker;

  constructor(tripId, startPosition) {
    this.tripId = tripId;
    this.startPosition = startPosition;
  }
}
