export class TripModel {
  _id: string;
  name: string;
  date: Date;
  destination: string;
  creatorId: string;
  description: string;

  constructor(_id, name, date, destination, creatorId, description) {
    this._id = _id;
    this.name = name;
    this.date = date;
    this.destination = destination;
    this.creatorId = creatorId;
    this.description = description;
  }
}
