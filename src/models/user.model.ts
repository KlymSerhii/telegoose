export class UserModel {
  facebooktoken: string;
  name: string;
  image: string;
  email: string;
  id: string;

  constructor(facebooktoken: string, name: string, image: string, email?: string, id?: string) {
    this.facebooktoken = facebooktoken;
    this.name = name;
    this.image = image;
    this.email = email;
    this.id = id;
  }
}
