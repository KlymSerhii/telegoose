import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AuthorizationPage } from './authorization';
import {DriverOrGoosePageModule} from "../driver-or-goose/driver-or-goose.module";

@NgModule({
  declarations: [
    AuthorizationPage,
  ],
  imports: [
    IonicPageModule.forChild(AuthorizationPage),
    DriverOrGoosePageModule
  ],
  exports: [
    AuthorizationPage
  ]
})
export class AuthorizationPageModule {}
