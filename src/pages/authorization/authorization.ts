import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { AuthService } from "angular2-social-login";
import { UserService } from '../../services/user.service';
import {UserModel} from "../../models/user.model";
import {SocketService} from "../../services/socket.service";

declare var Auth0Lock: any;

@IonicPage()
@Component({
  selector: 'page-authorization',
  templateUrl: 'authorization.html',
})
export class AuthorizationPage {

  socket: any;
  sub: any;
  constructor(public _auth: AuthService,
              private localStorage: Storage,
              private userService: UserService,
              private socketService: SocketService,
              private navCtrl: NavController) {
    this.socket = this.socketService.getSocket();
  }

  ionViewDidLoad() {
    this.localStorage.get('user').then((value) => {
      this.userService.setCurrentUser(value);
      if (value) this.navCtrl.push('StartTripPage');
    });
  }

  signIn(){
      this.sub = this._auth.login('facebook').subscribe(
        (data: any) => {
                    this.socket.emit('user:createOrUpdate', {image: data.image, name: data.name,
                      provider:"facebook", facebooktoken: data.token});
                    this.socket.on('user:createOrUpdateResponse', (dataNew) => {
                      let newUser = new UserModel(data.uid, data.name, data.image, data.email, dataNew._id);
                      this.localStorage.set('user', newUser);
                      this.userService.setCurrentUser(newUser);
                      if (dataNew) this.navCtrl.push('StartTripPage');
                      this.socket.removeAllListeners('user:createOrUpdateResponse');
                    });
                  }
      )
    }

  logout(){
      this._auth.logout().subscribe(
        (data)=>{
          this.localStorage.remove('user').then((value) => {
            this.userService.setCurrentUser(null);
          });
        }
      )
    }
}
