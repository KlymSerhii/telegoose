import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CarCreationPage } from './car-creation';

@NgModule({
  declarations: [
    CarCreationPage,
  ],
  imports: [
    IonicPageModule.forChild(CarCreationPage),
  ],
  exports: [
    CarCreationPage
  ]
})
export class CarCreationPageModule {}
