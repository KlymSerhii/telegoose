import { Component } from '@angular/core';
import { IonicPage, ViewController } from 'ionic-angular';

/**
 * Generated class for the CarCreationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-car-creation',
  templateUrl: 'car-creation.html',
})
export class CarCreationPage {

  constructor(public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
  }

  returnBack(e: Event) {
    e.preventDefault();
    this.viewCtrl.dismiss();
  }

  onAddCar(value: any) {
    console.log(value);
    //here give data to server
    this.viewCtrl.dismiss();
  }

}
