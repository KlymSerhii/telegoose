import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CarSelectionPage } from './car-selection';
import {CarCreationPageModule} from "../car-creation/car-creation.module";

@NgModule({
  declarations: [
    CarSelectionPage,
  ],
  imports: [
    IonicPageModule.forChild(CarSelectionPage),
    CarCreationPageModule
  ],
  exports: [
    CarSelectionPage
  ]
})
export class CarSelectionPageModule {}
