import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import {TripModel} from "../../models/trip.model";

@IonicPage()
@Component({
  selector: 'page-car-selection',
  templateUrl: 'car-selection.html',
})
export class CarSelectionPage {
  tripInfo: TripModel;
  cars = [
    {title: 'Ford Fiesta',
     seats: 4,
     consumption: 6.5},
    {title: 'Seat Cordoba',
      seats: 5,
      consumption: 7.5},
    {title: 'Ford Fiesta',
      seats: 4,
      consumption: 6.5},
    {title: 'Seat Cordoba',
      seats: 5,
      consumption: 7.5},
    {title: 'Ford Fiesta',
      seats: 4,
      consumption: 6.5},
    {title: 'Seat Cordoba',
      seats: 5,
      consumption: 7.5}
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams, private modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    //car loading here
    if (this.navParams.data.tripInfo) {
      this.tripInfo = this.navParams.data.tripInfo;
    }
  }

  returnBack() {
    this.navCtrl.pop();
  }

  addCar() {
    let modal = this.modalCtrl.create('CarCreationPage');
    modal.present();
  }

  selectCar(car: {title: string, seats: number, consumption: number}) {
    this.navCtrl.push('MapViewPage', {tripInfo: this.tripInfo, car:car, state:'DRIVER', markers:[JSON.parse(this.tripInfo.destination)]})
  }

}
