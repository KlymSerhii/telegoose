import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DriverOrGoosePage } from './driver-or-goose';
import {CarSelectionPageModule} from "../car-selection/car-selection.module";
import {JoinTripPageModule} from "../join-trip/join-trip.module";

@NgModule({
  declarations: [
    DriverOrGoosePage,
  ],
  imports: [
    IonicPageModule.forChild(DriverOrGoosePage),
    CarSelectionPageModule,
    JoinTripPageModule
  ],
  exports: [
    DriverOrGoosePage
  ]
})
export class DriverOrGoosePageModule {}
