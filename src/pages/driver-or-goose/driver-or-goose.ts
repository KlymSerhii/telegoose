import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {TripModel} from "../../models/trip.model";

@IonicPage()
@Component({
  selector: 'page-driver-or-goose',
  templateUrl: 'driver-or-goose.html',
})
export class DriverOrGoosePage {
  type: string = '';
  tripInfo: TripModel;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad DriverOrGoosePage');
    if (this.navParams.data.tripInfo) {
      this.tripInfo = this.navParams.data.tripInfo;
    }
  }

  clickOnImg(type: string) {

    if (this.type === type) {

      this.type = '';

    } else {
      this.type = type;
    }
  }

  onDriverOrGooseNext() {
    if (this.type === 'car') {
      this.navCtrl.push('CarSelectionPage', {tripInfo: this.tripInfo });
    } else {
      this.navCtrl.push('MapViewPage', {state: 'GOOSE',tripInfo: this.tripInfo, markers:[JSON.parse(this.tripInfo.destination)] });
    }
  }

}
