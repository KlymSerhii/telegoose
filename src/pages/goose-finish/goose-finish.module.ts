import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GooseFinishPage } from './goose-finish';

@NgModule({
  declarations: [
    GooseFinishPage,
  ],
  imports: [
    IonicPageModule.forChild(GooseFinishPage),
  ],
  exports: [
    GooseFinishPage
  ]
})
export class GooseFinishPageModule {}
