import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {SocketService} from "../../services/socket.service";
import {UserService} from "../../services/user.service";
import {TripService} from "../../services/trip.service";

/**
 * Generated class for the GooseFinishPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-goose-finish',
  templateUrl: 'goose-finish.html',
})
export class GooseFinishPage {

  tripId: string;
  startCoords: string;
  socket: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private socketService: SocketService,
              private userService: UserService, private tripService: TripService) {
    let tmp = null;
    tmp = this.navParams.get('goosePoint');
    console.log(tmp);
    this.tripId = tmp.tripId;
    this.startCoords = JSON.stringify(tmp.startPosition);
    this.socket = this.socketService.getSocket();
    this.socket.emit('goose:createOrUpdate', {tripId: this.tripId,
      selectedDrive: this.startCoords,
      personalData: this.userService.getCurrentUser().id});

    this.socket.on('goose:createOrUpdateResponse',(data: any) => {
      this.socket.removeAllListeners('goose:createOrUpdateResponse');
    });
  }

  ionViewDidLoad() {
  }

  onFinish() {
    this.navCtrl.push('GooseViewDriverPage');
  }

}
