import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GooseViewDriverPage } from './goose-view-driver';

@NgModule({
  declarations: [
    GooseViewDriverPage,
  ],
  imports: [
    IonicPageModule.forChild(GooseViewDriverPage),
  ],
  exports: [
    GooseViewDriverPage
  ]
})
export class GooseViewDriverPageModule {}
