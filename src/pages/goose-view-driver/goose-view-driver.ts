import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {UserModel} from "../../models/user.model";
import {DriverTripModel} from "../../models/driver-trip.model";


@IonicPage()
@Component({
  selector: 'page-goose-view-driver',
  templateUrl: 'goose-view-driver.html',
})
export class GooseViewDriverPage {
  tripName = 'hovno';
  driverTrip:DriverTripModel;
  carName = 'Ford Fiesta';
  numbOfSeats = 'four seats';
  guests = 'Anton, Oleg, Kobita';
  seats: UserModel[] = [{facebooktoken: 'rgeg', name: 'bibika', image:'assets/img/goose2.png', email: '', id:''},
    {facebooktoken: 'rgeg', name: 'hihika', image:'assets/img/goose2.png', email: '', id:''},
    {facebooktoken: 'rgeg', name: 'goose2', image:'assets/img/goose2.png', email: '', id:''}];
  date: Date = new Date(2017, 6, 18, 20);

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    if(this.navParams.data.driverTrip){
      this.driverTrip = this.navParams.data.driverTrip;
    }
  }

}
