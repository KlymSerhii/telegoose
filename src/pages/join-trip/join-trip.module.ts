import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JoinTripPage } from './join-trip';

@NgModule({
  declarations: [
    JoinTripPage,
  ],
  imports: [
    IonicPageModule.forChild(JoinTripPage),
  ],
  exports: [
    JoinTripPage
  ]
})
export class JoinTripPageModule {}
