import { Component } from '@angular/core';
import { FormBuilder} from "@angular/forms";
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { SocketService } from "../../services/socket.service";
import { AlertController } from 'ionic-angular';
import {UserModel} from "../../models/user.model";
import {TripModel} from "../../models/trip.model";
import {UserService} from "../../services/user.service";
import {TripService} from "../../services/trip.service";

@IonicPage()
@Component({
  selector: 'page-join-trip',
  templateUrl: 'join-trip.html',
})
export class JoinTripPage {
  socket: any;
  tripInfo: TripModel;
  creator: UserModel;
  trip: any;
  tripId: any;

  constructor(public navCtrl: NavController,
              public viewCtrl: ViewController,
              public navParams: NavParams,
              private userService: UserService,
              private socketService: SocketService,
              public alertCtrl: AlertController,
              private tripService: TripService) {
    this.socket = this.socketService.getSocket();
    let tripId = this.navParams.get('tripId');
    if (tripId) {
      this.navCtrl.push('TripInfoPage', {tripId: tripId, status: 'tap'});
    }
  }

  ionViewDidLoad() {
  }

  onJoinTrip(value: {tripId: string}) {
    this.getTripInfo(value.tripId);
  }

  getTripInfo(tripId: string): void {
    this.socket.emit('travel:getById', {_id: tripId});
    this.socket.on('travel:getByIdResponse', this.socketFunction.bind(this));
  }

  socketFunction(trip) {
    this.socket.removeEventListener('travel:getByIdResponse', this.socketFunction);
    //tripId example: 5946b3b201c5d814c07501ab
    if(!trip || !trip._id) {
      let alert = this.alertCtrl.create({
        title: 'Trip not found!',
        subTitle: 'There is no trip with such id! Please, try another one.',
        buttons: ['OK']
      });
      alert.present();
    } else {
      this.tripInfo = new TripModel(trip._id, trip.name, trip.endedDate, trip.endPointer,
        trip.creatorId, trip.description);

      this.socket.emit('user:getByFaceboock', {token: this.tripInfo.creatorId});

      this.socket.on('user:getByFaceboockResponse', (data) =>{
        data =  data || this.userService.getCurrentUser();
        if(!data || !data.id) {


            let alertUser = this.alertCtrl.create({
              title: 'User not found!',
              subTitle: 'There is no creator in this trip! Please, try another trip.',
              buttons: ['OK']
            });
            alertUser.present();
         // data =  this.userService.getCurrentUser();
         } else {
          this.creator = new UserModel(data.id, data.name, data.image);

          this.trip = {
            _id: trip._id,
            name: this.tripInfo.name,
            date: this.tripInfo.date,
            destination: this.tripInfo.destination,
            creator: this.creator,
            description: this.tripInfo.description
          };
          this.tripService.saveTripLocal(this.trip);
          this.navCtrl.push('TripInfoPage',
            {tripId: this.tripId, status: 'tap', trip: this.trip, creator: this.creator, tripInfo: this.tripInfo});
         }
        this.socket.removeAllListeners('user:getByFaceboockResponse');
      });
    }
    this.socket.removeAllListeners('travel:getByIdResponse');
  }

  returnBack(e: Event) {
    e.preventDefault();
    this.viewCtrl.dismiss();
  }

}
