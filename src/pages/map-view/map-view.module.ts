import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {MapViewPage} from './map-view';
import {AgmCoreModule, GoogleMapsAPIWrapper} from "angular2-google-maps/core";
import {DirectionsMapDirective} from "../../directive/route.directive";
import {TripInfoPageModule} from "../trip-info/trip-info.module";
import {GooseFinishPageModule} from "../goose-finish/goose-finish.module";

@NgModule({
  declarations: [
    MapViewPage,
    DirectionsMapDirective
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    IonicPageModule.forChild(MapViewPage),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBLv-PHIDjfxeVCA_EHNozFhbREhyNqHOE',
      libraries: ["places"]
    }),
    TripInfoPageModule,
    GooseFinishPageModule
  ],
  exports: [
    MapViewPage
  ],
  providers : [ GoogleMapsAPIWrapper ]
})
export class MapViewPageModule {
}
