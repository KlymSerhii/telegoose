import {Component, ElementRef, NgZone, OnInit, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {marker} from "../../models/marker.model";
import {Geolocation} from '@ionic-native/geolocation';
import {DirectionsMapDirective} from "../../directive/route.directive";
import {FormControl} from "@angular/forms";
import {MapsAPILoader} from "angular2-google-maps/core";
import {TripModel} from "../../models/trip.model";
import {DriverTripModel} from "../../models/driver-trip.model";


declare var google: any;

@IonicPage()
@Component({
  selector: 'page-map-view',
  templateUrl: 'map-view.html',
})
  export class MapViewPage implements OnInit {
  @ViewChild(DirectionsMapDirective) vc: DirectionsMapDirective;
  @ViewChild("pickupInput")
  public pickupInputElementRef: ElementRef;

  @ViewChild("pickupOutput")
  public pickupOutputElementRef: ElementRef;
  location: { lat: number, lng: number } = {lat: 0, lng: 0};
  markers: marker[] = [];
  title: string;
  state: string;
  placeholder: string = "Enter location";
  tripInfo:TripModel;
  submitLabel:string;
  private allowAddMarker:boolean;
  public destinationInput: FormControl;
  public destinationOutput: FormControl;

  constructor(private navParams: NavParams,
              private mapsAPILoader: MapsAPILoader,
              private ngZone: NgZone,
              private navCtrl: NavController,
              private geolocation: Geolocation) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapViewPage');
    this.title = this.navParams.data.title || 'Select end position:';
    this.state = this.navParams.data.state;
    this.allowAddMarker = true;
    this.submitLabel = "Next >";
    this.setPlaceholder();
    //DO map state switch logic here
    if(this.navParams.data.tripInfo){
      this.tripInfo = this.navParams.data.tripInfo;
      let destination;
      if (typeof(this.tripInfo.destination) === 'string' ) {
        destination = JSON.parse(this.tripInfo.destination);
      } else {
        destination = this.tripInfo.destination;
      }
      this.markers.push(destination);
      this.location = destination;
    }
    if(this.state==='DRIVER'){
      this.title = 'Select where you drive from';
    }
    if(this.state==='GOOSE'){
      this.title = 'Select your pick location';
    }
    if(this.state==='ROUTE'){
      this.title = 'Here is your route';
      this.submitLabel = "Start!";
    }
    if (this.navParams.data.markers) {
      this.markers = this.navParams.data.markers;
      this.tryDrawRoutes();
    } else {
      this.geolocation.getCurrentPosition()
        .then(
          (location) => {
            this.location.lat = location.coords.latitude;
            this.location.lng = location.coords.longitude;
          }
        )
    }
  }

  ngOnInit() {
    this.destinationInput = new FormControl();
    this.destinationOutput = new FormControl();
    let autocompleteInput: any;
    let autocompleteOutput: any;
    this.mapsAPILoader.load().then(() => {
      if(this.pickupInputElementRef) {
        autocompleteInput = new google.maps.places.Autocomplete(this.pickupInputElementRef.nativeElement, {
          types: ["address"]
        });
        this.setupPlaceChangedListener(autocompleteInput, 'ORG');
      }
      if(this.pickupOutputElementRef) {
        autocompleteOutput = new google.maps.places.Autocomplete(this.pickupOutputElementRef.nativeElement, {
          types: ["address"]
        });
        this.setupPlaceChangedListener(autocompleteOutput, 'DES');
      }

    });
  }

  private setupPlaceChangedListener(autocomplete: any, mode: any) {
    autocomplete.addListener("place_changed", () => {
      this.ngZone.run(() => {
        let place: any = autocomplete.getPlace();
        //verify result
        if (place.geometry === undefined) {
          return;
        }
        //TODO: make origin and destination markers always 0 and 1 element of array
        if (this.state=='NEW_TRIP') {
          this.setEndPoint(place.geometry.location.lat(), place.geometry.location.lng());
        } else {
          this.setStartPoint(place.geometry.location.lat(), place.geometry.location.lng());
        }

        if (this.vc.directionsDisplay === undefined) {
          this.mapsAPILoader.load().then(() => {
            this.vc.directionsDisplay = new google.maps.DirectionsRenderer;
          });
        }
        // this.tryDrawRoutes();
      });
    });
  }

  setPlaceholder(){
    if(this.state=='NEW_TRIP'){
      this.placeholder = 'Enter destination point';
    }
    if(this.state=='DRIVER'){
      this.placeholder = 'Enter your start location';
    }
    if(this.state=='GOOSE'){
      this.placeholder = 'Enter your wanted join location';
    }
  }

  setEndPoint(lat: number, lng: number) {
    this.markers[0] = {lat: lat, lng: lng, draggable: true};
    this.location = {lat: this.markers[0].lat, lng: this.markers[0].lng};
  }

  setStartPoint(lat: number, lng: number) {
    this.markers[1] = {lat: lat, lng: lng, draggable: true};
    this.location = {lat: this.markers[1].lat, lng: this.markers[1].lng};
  }

  mapClicked($event: any) {
    if(this.allowAddMarker) {
      this.addMarker($event.coords.lat, $event.coords.lng);
    }
    this.allowAddMarker = false;
  }

  addMarker(lat: number, lng: number) {
    this.markers.push({
      lat: lat,
      lng: lng,
      draggable: true
    });
  }

  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }

  markerDragEnd(m, $event: {coords: {lat: number, lng: number}}) {
    console.log('dragEnd', m, $event);
    m.lat = $event.coords.lat;
    m.lng = $event.coords.lng;
  }

  onNextClick() {
    // this.tryDrawRoutes();
    this.doMapAction();
  }

  tryDrawRoutes() {
    if (this.markers.length > 1) {
      this.vc.updateRoutes(this.markers[0], this.markers[1], this.markers);     //TODO: change to place with description, not just coords
    }
  }

  navigateTo(view:string,params:object){
    this.navCtrl.push(view, params);
  }

  private doMapAction(){
    if(this.state==='NEW_TRIP' && this.markers.length){
      //TODO: remove this shit, find way how to get marker description
      this.doNewTripAction();
    }
    if(this.state==='DRIVER' && this.markers.length==2){
      this.doDriverAction();
    }

    if(this.state==='GOOSE' && this.markers.length==2){
      this.doGooseAction();
    }
  }

  private doDriverAction(){
    let driverTrip: DriverTripModel = new DriverTripModel(this.tripInfo._id, this.markers[1]);
    this.navigateTo('SelectGoosePage',{tripId: this.tripInfo._id, trip:driverTrip, tripInfo: this.tripInfo, markers:this.markers});
  }

  private doGooseAction(){
    let goosePoint: DriverTripModel = new DriverTripModel(this.tripInfo._id, this.markers[1]);
    this.navigateTo('GooseFinishPage',{tripId: this.tripInfo._id, goosePoint:goosePoint});
  }

  private doNewTripAction(){
    this.vc.mapsAPIWrapper.getNativeMap().then(map => {
      let directionsService = new google.maps.DirectionsService;
      let directionsDisplay = new google.maps.DirectionsRenderer;
      let origin = new google.maps.LatLng(this.markers[0].lat, this.markers[0].lng);
      let destination = new google.maps.LatLng(this.markers[0].lat, this.markers[0].lng);
      directionsDisplay.setMap(map);
      directionsService.route({
        origin: origin,
        destination: destination,
        optimizeWaypoints: true,
        travelMode: 'DRIVING',

      },(res: any, status: any) => {
        if (status === 'OK') {
          console.log("STATUS WAS OK");
          console.log('result', res);
          this.navigateTo('TripInfoPage', {destination: this.markers[0], destDescr:res.routes[0].summary, status:'create'});
          return res;
          // directionsDisplay.setDirections(res);
        } else {
          window.alert('Directions request failed due to ' + status);
        }
      })
    })
  }

}
