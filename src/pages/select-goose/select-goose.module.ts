import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectGoosePage } from './select-goose';
import {DragulaModule} from "ng2-dragula";

@NgModule({
  declarations: [
    SelectGoosePage,
  ],
  imports: [
    IonicPageModule.forChild(SelectGoosePage),
    DragulaModule
  ],
  exports: [
    SelectGoosePage
  ]
})
export class SelectGoosePageModule {}
