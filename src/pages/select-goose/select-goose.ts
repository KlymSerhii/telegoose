import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {UserModel} from "../../models/user.model";
import {DragulaService} from 'ng2-dragula/ng2-dragula';
import {SocketService} from "../../services/socket.service";
import {marker} from "../../models/marker.model";
import {DriverTripModel} from "../../models/driver-trip.model";



@IonicPage()
@Component({
  selector: 'page-select-goose',
  templateUrl: 'select-goose.html',
  providers: [DragulaService]
})
export class SelectGoosePage {
  socket: any;

  tripInfo: DriverTripModel;
  name: string;
  driveId: string;
  markers:marker[];

  gooses: UserModel[] = [];

  seats: UserModel[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public dragulaService: DragulaService, public socketService: SocketService) {

    this.socket = this.socketService.getSocket();

    this.tripInfo = this.navParams.get('trip');
    this.markers = this.navParams.get('markers');
    this.name = 'bibika';
    this.gooses = [];

    dragulaService.drop.subscribe((value) => {
      if (this.seats.length > 3) {
        this.gooses.push(this.seats[3]);
        this.seats.pop();
      }
    });

  }

  ionViewDidLoad() {
    this.getAllGoosesByTripId('5');

    if (this.tripInfo) {

      this.getAllGoosesByTripId(this.tripInfo.tripId);

    }
  }

  getAllGoosesByTripId(tripId: string): void {

    this.socket.emit('goose:getAllFree', {tripId: tripId});
    this.socket.on('goose:getAllFreeResponse', (data) => {

      let array: UserModel[] = [];
      this.driveId = data._id;
      // for (let datum of data) {
      //   console.log(datum);
      //   let persData = datum.personalData;
      //   array.push(new UserModel(persData.facebooktoken, persData.name, persData.image, ''));
      // }
      for (let i=1; i<4; i++){
        array.push(new UserModel('id_'+i, "Goose_"+i, 'assets/img/goose'+ i +'.png'))
      }

      this.gooses = array;
    });
  }

  selectRandomGooses(): void {

    let length = this.gooses.length;

    for (let i=0; i < length; i++) {
      this.seats.push(this.gooses[0]);
      this.gooses.shift();
    }
  }

  getRoute(): void {
    // for (let user of this.seats) {
    //   this.socket.emit('drive:offerToDrive', {_id: this.driveId, gooseId:user.facebooktoken});
    //   this.socket.on('drive:offerToDriveResponse', (data) => {
    //     console.log(data);
    //         this.navCtrl.push('MapViewPage',{state:'ROUTE',markers:goosesCoords});
    //     this.socket.removeAllListeners('drive:offerToDriveResponse');
    //   });
    // }

    //TODO: set default gooses coords
    let goosesCoords:any[] =
      [
        this.markers[0],
        this.markers[1],
        {lat: 50.489187003707336, lng: 30.497875213623047, draggable: true},
        {lat: 50.407642311287795, lng: 30.518646240234375, draggable: true},
        // {lat: 50.44722980684235, lng: 30.57941436767578, draggable: true},
        {lat: 50.4646072249993, lng: 30.644474029541016, draggable: true},
      ];
    this.navCtrl.push('MapViewPage',{state:'ROUTE',markers:goosesCoords});

  }


}
