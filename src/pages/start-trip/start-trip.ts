import {Component} from '@angular/core';
import {IonicPage, NavController, ModalController} from 'ionic-angular';
import {MapViewPage} from "../map-view/map-view";
import {TripService} from "../../services/trip.service";

@IonicPage()
@Component({
  selector: 'page-start-trip',
  templateUrl: 'start-trip.html',
})
export class StartTripPage {

  constructor(public navCtrl: NavController,
              private tripService: TripService,
              private modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
  }

  onCreateTrip() {
    this.navCtrl.push('MapViewPage', {state: "NEW_TRIP", title: "Select end position:"});
  }

  onStartTrip() {
    let modal = this.modalCtrl.create('JoinTripPage');
    modal.present();
  }

}
