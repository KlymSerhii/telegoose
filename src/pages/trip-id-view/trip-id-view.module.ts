import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TripIdViewPage } from './trip-id-view';

@NgModule({
  declarations: [
    TripIdViewPage,
  ],
  imports: [
    IonicPageModule.forChild(TripIdViewPage),
  ],
  exports: [
    TripIdViewPage
  ]
})
export class TripIdViewPageModule {}
