import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-trip-id-view',
  templateUrl: 'trip-id-view.html',
})
export class TripIdViewPage {
  tripInfo: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.tripInfo = this.navParams.get('tripInfo');
  }

  ionViewDidLoad() {
  }

  onTripStart() {
    this.navCtrl.push('DriverOrGoosePage',{tripInfo: this.tripInfo});
  }

}
