import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {TripModel} from "../../models/trip.model";
import {UserService} from "../../services/user.service";
import {UserModel} from "../../models/user.model";
import {SocketService} from "../../services/socket.service";
import {TripService} from "../../services/trip.service";

@IonicPage()
@Component({
  selector: 'page-trip-info',
  templateUrl: 'trip-info.html',
})
export class TripInfoPage {
  trip: FormGroup;
  socket: any;
  creator: UserModel;
  tripId: string;

  tripInfo: TripModel;
  status: string;


  constructor(public navCtrl: NavController, public navParams: NavParams,
              private formBuilder: FormBuilder, private userService: UserService,
              public socketService: SocketService, private tripService: TripService) {

    this.socket = this.socketService.getSocket();

    this.status = this.navParams.get('status');
    if (this.status !== 'tap') {
      let destination = this.navParams.get('destination');
      let month: string = (new Date().getMonth() + 1) + '';
      if (month.length === 1) {
        month = '0' + month;
      }

      let day: string = new Date().getDate() + '';
      if (day.length === 1) {
        day = '0' + day;
      }

      let currDate: string = new Date().getFullYear() + '-' + month + '-' + day + 'T10:00:00Z';

      this.trip = this.formBuilder.group({
        name: ['', Validators.required],
        date: [currDate, Validators.required],
        destination: [destination, Validators.required],
        description: ['', Validators.required],
      });
    } else {

      this.tripId = this.navParams.get('tripId');

    }
    if (this.status !== 'tap') {

      this.creator = this.getCurrentUser();

    } else {

      this.tripInfo = this.navParams.get('tripInfo');
      this.creator = this.navParams.get('creator');
      let tripData = this.navParams.get('trip');
      this.trip = this.formBuilder.group({
        name: [tripData.name, Validators.required],
        date: [tripData.date, Validators.required],
        creator: [tripData.creator.name, Validators.required],
        destination: [tripData.destination, Validators.required],
        description: ['some description here', Validators.required]
      });
      console.log(tripData.description);
    }
  }

  ionViewDidLoad() {
  }

  getCurrentUser(): UserModel {
    return this.userService.getCurrentUser();
  }

  goBack(): void {
    this.navCtrl.pop();
  }

  onAddTrip(): void {

    if (this.status !== 'tap') {

      this.tripInfo = new TripModel('', this.trip.value.name, this.trip.value.date, this.trip.value.destination,
        this.creator.facebooktoken, this.trip.value.description);

      this.addTrip();

    } else {

      this.navCtrl.push('DriverOrGoosePage', {tripInfo: this.tripInfo });

    }
  }

  addTrip(): void {
    let trip = this.tripInfo;
    this.socket.emit('travel:createOrUpdate', {
      endPointer: JSON.stringify(trip.destination),
      name: trip.name,
      creatorId: trip.creatorId,
      description: trip.description,
      endedDate: trip.date
    });

    this.socket.on('travel:createOrUpdateResponse', (data) => {
      this.tripInfo._id = data._id;
      this.tripService.saveTripLocal(this.tripInfo);
      this.socket.removeAllListeners('travel:createOrUpdateResponse')
      this.navCtrl.push('TripIdViewPage', {tripInfo: this.tripInfo});
    });
  }

}
