
import {Injectable} from "@angular/core";

import * as io from 'socket.io-client';
@Injectable()
export class SocketService {

  private socket: any;

  constructor() {
    console.log('SocketService constructor');
    this.socket = io('http://192.168.1.225:3333');

    this.socket.on('message', (msg) => {
      console.log("message", msg);
    });
  }

  getSocket(){
    return this.socket;
  }
}
