import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import {Response, Http, Headers} from "@angular/http";
import {TripModel} from "../models/trip.model";
import {UserModel} from "../models/user.model";
import { Storage } from "@ionic/storage";


@Injectable()
export class TripService {
  myTrip:any;
  constructor(private http: Http,
              private storage: Storage) {

  }


  addTrip(trip: TripModel): Observable<any> {
    const body = JSON.stringify(trip);
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.post("https://kpi-elective.herokuapp.com/addElective", body, {headers: headers})
      .map((response: Response) => {
        const trip = response.json();

        return trip._id;
      })
  }

  getTrip(): any {
    return this.myTrip;
  }

  saveTripLocal(trip:any){
    this.myTrip = JSON.stringify(trip);
    this.storage.set('myTrip', this.myTrip);
  }

  getTripLocal(){
    return this.storage.get('myTrip')
      .then((myTrip) => {
      this.myTrip = JSON.parse(myTrip);
      return this.myTrip.markers;
    });
  }

  getAllGoosesByTripId(tripId: string): Observable<any> {
    return this.http.get('')
      .map((response: Response) => {
        const gooses = response.json();
        let transformedGooses: UserModel[] = [];

        for (let goose of gooses) {

          let newGoose = new UserModel(goose.facebooktoken, goose.name, goose.image, '');

          transformedGooses.push(newGoose);
        }

        return transformedGooses;
      });
  }
}
