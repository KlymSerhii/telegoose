import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import {Response, Http, Headers} from "@angular/http";
import {UserModel} from "../models/user.model";


@Injectable()
export class UserService {

  private user: UserModel = null;

  constructor(private http: Http) {

  }

  getCurrentUser(): UserModel {
    return this.user;
  }

  setCurrentUser(user: UserModel) {
    this.user = user;
  }

  hasUser(): boolean {
    return this.user !== null;
  }

}
